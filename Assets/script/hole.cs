using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class hole : MonoBehaviour
{
    public Camera cam;
    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("arrow"))
        {
            StartCoroutine(camerashake(0.5f, 0.4f));
        }
    }
    IEnumerator camerashake(float shaketime, float magnitude)// KAMERA T�TREMES� (CAMRE SHAKE)
    {
        Vector3 originalpos = cam.transform.localPosition;

        float spendtime = 0;

        while (spendtime < shaketime)
        {
            float newposx = Random.Range(1, 3) * magnitude;
            float newposy = Random.Range(1, 1) * magnitude;

            cam.transform.localPosition = new Vector3(newposx, newposy, originalpos.z);
            spendtime += Time.deltaTime;
            yield return null;
        }

        cam.transform.localPosition = originalpos;
    }
}
